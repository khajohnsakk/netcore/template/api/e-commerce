﻿using System;
using System.Collections.Generic;

namespace Demo.Persistence;

public partial class Todo
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public bool? IsComplete { get; set; }

    public string? Category { get; set; }

    public virtual Category? CategoryNavigation { get; set; }
}
