﻿using System;
using System.Collections.Generic;

namespace Demo.Persistence;

public partial class Category
{
    public string Code { get; set; } = null!;

    public string? Name { get; set; }

    public virtual ICollection<Todo> Todos { get; set; } = new List<Todo>();
}
