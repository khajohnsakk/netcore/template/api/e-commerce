﻿using Demo.Application.DTOs.Email;

namespace Demo.Application.Interfaces
{
    public interface IEmailService
    {
        Task SendAsync(EmailRequestDto request);
    }
}
