﻿using AutoMapper;

using Demo.Application.Common.Exceptions;
using Demo.Application.Interfaces.Repositories;
using Demo.Domain.Entities;
using Demo.Shared;

using MediatR;

namespace Demo.Application.Features.Players.Commands.UpdatePlayer
{
    public record UpdatePlayerCommand : IRequest<Result<int>>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ShirtNo { get; set; }
        public string PhotoUrl { get; set; }
        public DateTime? BirthDate { get; set; }
    }

    internal class UpdatePlayerCommandHandler : IRequestHandler<UpdatePlayerCommand, Result<int>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UpdatePlayerCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper; 
        }

        public async Task<Result<int>> Handle(UpdatePlayerCommand command, CancellationToken cancellationToken)
        {
            var player = await _unitOfWork.Repository<Player>().GetByIdAsync(command.Id);
            if (player != null)
            {
                player.Name = command.Name;
                player.ShirtNo = command.ShirtNo;
                player.PhotoUrl = command.PhotoUrl;
                player.BirthDate = command.BirthDate;

                await _unitOfWork.Repository<Player>().UpdateAsync(player);
                player.AddDomainEvent(new PlayerUpdatedEvent(player));

                await _unitOfWork.Save(cancellationToken);

                return await Result<int>.SuccessAsync(player.Id, "Player Updated.");
            }
            else
            {
                return await Result<int>.FailureAsync("Player Not Found.");
            }               
        }
    }
}

//try
//{
//    //First, Begin the Transaction
//    unitOfWork.CreateTransaction();
//    if (ModelState.IsValid)
//    {
//        //Do the Database Operation
//        genericRepository.Insert(model);
//        //Call the Save Method to call the Context Class Save Changes Method
//        unitOfWork.Save();
//        //Do Some Other Tasks with the Database
//        //If everything is working then commit the transaction else rollback the transaction
//        unitOfWork.Commit();
//        return RedirectToAction("Index", "Employee");
//    }
//}
//catch (Exception ex)
//{
//    //Log the exception and rollback the transaction
//    unitOfWork.Rollback();
//}
