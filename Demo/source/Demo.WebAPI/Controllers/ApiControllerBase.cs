﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Demo.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public abstract class ApiControllerBase : ControllerBase
    {
       
    }
}
