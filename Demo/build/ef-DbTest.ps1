# Allow specifying a namespace for scaffolding 
# https://github.com/aspnet/EntityFrameworkCore/issues/9977
# https://github.com/aspnet/EntityFrameworkCore/issues/9723
# dotnet tool update --global dotnet-ef

cd ..\source
$connectionString = ""
get-childitem ".\Demo.WebAPI\appsettings.Development.json" | % {
  $temp = [System.IO.File]::ReadAllText($_.FullName).Split([Environment]::NewLine)[4]
  $temp = $temp.Split(':')[1]
  $connectionString = $temp -replace '"', ""
}

echo "connectionString : $connectionString"

#dotnet ef dbcontext -v scaffold "Server=.;Database=DbTest;user id=sa;password=root;TrustServerCertificate=True" Microsoft.EntityFrameworkCore.SqlServer  -c DbTestContext --context-dir "Contexts" --output-dir "..\Demo.Domain\Entities"  --startup-project "Demo.Persistence" -f --no-build 
dotnet ef dbcontext -v scaffold $connectionString Microsoft.EntityFrameworkCore.SqlServer  -c DbTestContext --context-dir "Contexts" --output-dir "..\Demo.Domain\Entities"  --startup-project "Demo.Persistence" -f --no-build 

get-childitem ".\Demo.Domain\Entities" | where {$_.extension -eq ".cs"} | % {
     
	 $content = [System.IO.File]::ReadAllText($_.FullName).Replace("namespace Demo.Persistence","namespace Demo.Domain.Entities")
	 [System.IO.File]::WriteAllText($_.FullName, $content)
}

get-childitem ".\Demo.Persistence\Contexts" | where {$_.Name -eq "DbTestContext.cs"} | % {
     
	 $content = [System.IO.File]::ReadAllText($_.FullName).Replace("using System;","using Demo.Domain.Entities;").Replace("using Demo.Persistence;","")
	 [System.IO.File]::WriteAllText($_.FullName, $content)
}

cd ..\build